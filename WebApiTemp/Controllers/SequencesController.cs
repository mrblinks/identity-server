﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using IdentityServer.Models;

namespace IdentityServer.Controllers
{
    [Produces("application/json")]
    [Route("api/Sequences")]
    public class SequencesController : Controller
    {
        private readonly AppDbContext _context;

        public SequencesController(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/Sequences
        [HttpGet]
        public IEnumerable<Sequence> GetSequence()
        {
            return _context.Sequence;
        }

        // GET: api/Sequences/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSequence([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var sequence = await _context.Sequence.SingleOrDefaultAsync(m => m.SequenceId == id);

            if (sequence == null)
            {
                return NotFound();
            }

            return Ok(sequence);
        }

        // PUT: api/Sequences/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSequence([FromRoute] int id, [FromBody] Sequence sequence)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != sequence.SequenceId)
            {
                return BadRequest();
            }

            _context.Entry(sequence).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SequenceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Sequences
        [HttpPost]
        public async Task<IActionResult> PostSequence([FromBody] Sequence sequence)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Sequence.Add(sequence);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSequence", new { id = sequence.SequenceId }, sequence);
        }

        // DELETE: api/Sequences/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSequence([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var sequence = await _context.Sequence.SingleOrDefaultAsync(m => m.SequenceId == id);
            if (sequence == null)
            {
                return NotFound();
            }

            _context.Sequence.Remove(sequence);
            await _context.SaveChangesAsync();

            return Ok(sequence);
        }

        private bool SequenceExists(int id)
        {
            return _context.Sequence.Any(e => e.SequenceId == id);
        }
    }
}