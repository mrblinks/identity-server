﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using IdentityServer.Models;
using IdentityServer.MyServices;
using Microsoft.AspNetCore.Identity;
using IdentityServer.Extensions;

namespace IdentityServer.Controllers
{
    [Produces("application/json")]
    [Route("api/Users")]
    public class UsersController : Controller
    {
        private readonly AppDbContext _context;
        private readonly UserManager<AppUsers> _userManager;
        private readonly IEmailSender _emailSender;

        public UsersController(UserManager<AppUsers> usrMgr, AppDbContext context, IEmailSender emailSender)
        {
            _userManager = usrMgr;
            _context = context;
            _emailSender = emailSender;
        }

        // GET: api/Users
        [HttpGet]
        public IEnumerable<User> GetUser()
        {
            return _context.User;
        }

        [HttpGet("Identity")]
        public IEnumerable<AppUsers> GetAppUser()
        {
            return _userManager.Users;
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetUser([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = await _context.User.SingleOrDefaultAsync(m => m.UserId == id);

            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        // PUT: api/Users/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUser([FromRoute] int id, [FromBody] User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != user.UserId)
            {
                return BadRequest();
            }

            _context.Entry(user).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Users
        //[HttpPost]
        //public async Task<IActionResult> PostUser([FromBody] User user)
        //{
        //    if (!ModelState.IsValid) return BadRequest(ModelState);
            
        //    _context.User.Add(user);
        //    await _context.SaveChangesAsync();

        //    return CreatedAtAction("GetUser", new { id = user.UserId }, user);
        //}

        // POST: api/Users
        [HttpPost]
        public async Task<IActionResult> PostUsers([FromBody] User user)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            
            var appusr = _userManager.Users.Any(u => u.UserName.Equals(user.Username));
            var appemail = _userManager.Users.Any(u => u.Email.Equals(user.Email));
            if ( appusr || appemail)
            {
                return BadRequest("Username or Email already taken");
            }
            user.CreatedDate = DateTime.Now;
            _context.User.Add(user);
            await _context.SaveChangesAsync();
            AppUsers appUser = new AppUsers
            {
                UserId = user.UserId, UserName = user.Username, Email = user.Email,LoginTime = DateTime.Now,
                LogOutTime = DateTime.Now,JoinDate = DateTime.Now,isLoggedIn = false,
            };
            IdentityResult result = await _userManager.CreateAsync(appUser, user.Password);

            if (!result.Succeeded) return BadRequest("Incorrect Username or password");
            
            var code = await _userManager.GenerateEmailConfirmationTokenAsync(appUser);
            var callbackUrl = Url.EmailConfirmationLink(appUser.Id, code, Request.Scheme);
            await _emailSender.SendEmailConfirmationAsync(user.Email, callbackUrl, user);

            return Ok(new { Status = "OK", Message = "Successfully Added", Output = "User has been added", User = user });
        }

        // DELETE: api/Users/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUser([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = await _context.User.SingleOrDefaultAsync(m => m.UserId == id);
            if (user == null)
            {
                return NotFound();
            }

            _context.User.Remove(user);
            await _context.SaveChangesAsync();

            return Ok(user);
        }

        private bool UserExists(int id)
        {
            return _context.User.Any(e => e.UserId == id);
        }
    }
}