﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using IdentityServer.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using IdentityServer.MyServices;
using Microsoft.AspNetCore.DataProtection;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.IO;
using Microsoft.EntityFrameworkCore;
using IdentityServer.Extensions;

namespace IdentityServer.Controllers
{
    [Route("api/")]
    public class DefaultController : Controller
    {
        private readonly UserManager<AppUsers> _userManager;
        private readonly SignInManager<AppUsers> _signInManager;
        private readonly IConfiguration _configuration;
        private readonly AppDbContext _context;
        private readonly ILogger _logger;
        private readonly IEmailSender _emailSender;
        private readonly IDataProtector _protector;

        public DefaultController(UserManager<AppUsers> usrMgr, SignInManager<AppUsers> signinMgr,
            IConfiguration configuration, AppDbContext context, ILogger<DefaultController> logger, IEmailSender emailSender, IDataProtectionProvider provider)
        {
            _userManager = usrMgr;
            _signInManager = signinMgr;
            _configuration = configuration;
            _context = context;
            _logger = logger;
            _emailSender = emailSender;
            _protector = provider.CreateProtector("Contoso.MyClass.v1");
        }
        
        [HttpPost("Login")]
        public async Task<IActionResult> Logins([FromBody] Login model)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var user = await _userManager.FindByNameAsync(model.Username);
            if (user == null)
            {
                user = await _userManager.FindByEmailAsync(model.Username);//.Include(b => b.User);
                if (user == null) return BadRequest("Invalid Username: user doesn't Exist");
            }
            var users = _context.User.Where(a => a.Username == model.Username || a.Email == model.Username)
               .Include(b => b.Company).FirstOrDefault();
            //if (users.Status.ToLower() == "disabled") return BadRequest("You have Been Disabled by Administrator please Contact Admin");
            if (users.Company.ExpiryDate < DateTime.Now) return BadRequest("Your Website License has Expired please Contact Acyst Tech for Renewal");

            var result = await _signInManager.CheckPasswordSignInAsync(user, model.Password, true);
            if (!result.Succeeded) return BadRequest($"Incorrect Password for {model.Username}");

            var userClaims = await _userManager.GetClaimsAsync(user);

            userClaims.Add(new Claim(JwtRegisteredClaimNames.UniqueName, user.UserName));
            userClaims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("qwertyuiopasdfghjklzxcvbnm1234567890"));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(
                issuer: _configuration.GetSection("AppSettings")["Url"],
                audience: "http://localhost:50888",
                claims: userClaims,
                expires: DateTime.Now.AddHours(12),
                signingCredentials: creds);
            
            user.LoginTime = DateTime.UtcNow;
            user.isLoggedIn = true;
            await _userManager.UpdateAsync(user);
            _context.SaveChanges();

            return Ok(new { access_token = new JwtSecurityTokenHandler().WriteToken(token), expires_in_hours = 12, date = DateTime.UtcNow, User = users });
        }
        
        [HttpPost("Logout/{username}")]
        public async Task<IActionResult> Logout(string username)
        {
            if (!ModelState.IsValid) return BadRequest();
            //var use = _context.Users.FirstOrDefault(a => a.UserName == username).User;// || 
            var user = await _userManager.FindByNameAsync(username);
            user.LogOutTime = DateTime.UtcNow;
            user.isLoggedIn = false;
            await _userManager.UpdateAsync(user);
            _context.SaveChanges();
            _logger.LogInformation($"User {username} logged out");

         return Ok("Logout successfull");
        }

        [HttpPost("Changepassword")]
        public async Task<IActionResult> PasswordChange([FromBody] ChangePassword model)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var appuser = await _userManager.FindByNameAsync(model.Username);
            //var user = _context.Users.FirstOrDefault(a => a.Username == model.Username);
            if (appuser == null) return NotFound("User does not exist");
            var resul = await _signInManager.CheckPasswordSignInAsync(appuser, model.OldPassword, true);
            if (!resul.Succeeded) return BadRequest("Current Password is incorrect");
            //if (model.OldPassword != user.Password) return BadRequest("Current Password is incorrect.");

            IdentityResult result = await _userManager.ChangePasswordAsync(appuser, model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                _context.SaveChanges();
                return Ok("Password Changed Successfully");
            }
            else
            {
                return Content("Could not change Password");
            }
        }

        [HttpGet("Resetuser/{id}")]
        public async Task<IActionResult> PasswordReset([FromRoute] int id)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var user = _context.User.FirstOrDefault(a => a.UserId == id);
            var appuser = await _userManager.FindByNameAsync(user.Username);
            if (appuser == null) return NotFound("User does not exist");
            var Password = DateTime.Now.ToString("ddhhmmss");
            appuser.EmailConfirmed = true;
            try
            {
                await _userManager.RemovePasswordAsync(appuser);
                await _userManager.AddPasswordAsync(appuser, Password);
                string passwordhash = _userManager.PasswordHasher.HashPassword(appuser, Password);
                appuser.PasswordHash = passwordhash;
                await _userManager.UpdateNormalizedEmailAsync(appuser);
                await _userManager.UpdateNormalizedUserNameAsync(appuser);
                await _userManager.UpdateSecurityStampAsync(appuser);
                await _userManager.UpdateAsync(appuser);
                await _context.SaveChangesAsync();

                var code = await _userManager.GenerateEmailConfirmationTokenAsync(appuser);
                var callbackUrl = Url.EmailConfirmationLink(appuser.Id, code, Request.Scheme);
                await _emailSender.SendEmailResetConfirmationAsync(appuser.Email, callbackUrl, user);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UsersExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(new { Status = "Ok", Message = "Password Reset Successfully", NewPassword = user.Password });
        }
        
        [HttpPost("Reset")]
        public async Task<IActionResult> PassWordReset([FromBody] Reset reset)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var user = _context.User.FirstOrDefault(a => a.Email == reset.Email);
            var appuser = await _userManager.FindByNameAsync(user.Username);
            if (appuser == null || user == null) return NotFound("User does not exist");
            user.Password = DateTime.Now.ToString("ddhhmmss");
            appuser.EmailConfirmed = true;

            try
            {
                await _userManager.RemovePasswordAsync(appuser);
                await _userManager.AddPasswordAsync(appuser, user.Password);
                string passwordhash = _userManager.PasswordHasher.HashPassword(appuser, user.Password);
                appuser.PasswordHash = passwordhash;
                await _userManager.UpdateNormalizedEmailAsync(appuser);
                await _userManager.UpdateNormalizedUserNameAsync(appuser);
                await _userManager.UpdateSecurityStampAsync(appuser);
                await _userManager.UpdateAsync(appuser);
                await _context.SaveChangesAsync();

                var code = await _userManager.GenerateEmailConfirmationTokenAsync(appuser);
                var callbackUrl = Url.EmailConfirmationLink(appuser.Id, code, Request.Scheme);
                await _emailSender.SendEmailResetConfirmationAsync(user.Email, callbackUrl, user);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UsersExists(user.UserId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return Ok(new { Status = "Ok", Message = "Account Reset Successfully", Output = "Check Mail for new Password" });
        }

        // PUT: api/Users/5
        [HttpPost("Image/{id}")]
        public async Task<IActionResult> PutImageUsers([FromRoute] int id, [FromBody] Profile image)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var user = _context.User.FirstOrDefault(a => a.UserId == id);
            if (user == null) return NotFound($"User with id {id} does not exist");

            user.Image = image.ProfileImage;
            _context.Entry(user).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return Ok(image);
        }

        [HttpGet("AccessToken")]
        [AllowAnonymous]
        public IActionResult AccessToken()
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("qwertyuiopasdfghjklzxcvbnm1234567890"));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(
                issuer: _configuration.GetSection("AppSettings")["Url"],
                audience: "http://localhost:50888",
                expires: DateTime.Now.AddMinutes(5),
                signingCredentials: creds);
            var response = new { access_token = new JwtSecurityTokenHandler().WriteToken(token), expires_in_Minutes = 5 };
            return Ok(response);
        }

        private bool UsersExists(int id)
        {
            return _context.Users.Any(e => e.UserId == id);
        }

        public class Login
        {
            [Required]
            public string Username { get; set; }
            [Required]
            public string Password { get; set; }
        }

        public class Profile
        {
            [Required]
            public string ProfileImage { get; set; }
        }

        public class Reset
        {
            [Required]
            public string Email { get; set; }
        }

        public class ChangePassword
        {
            [Required]
            public string Username { get; set; }
            [Required]
            public string OldPassword { get; set; }
            [Required]
            public string NewPassword { get; set; }
        }
        
    }
}
