﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Authorization;
using AcystTechApi.Models;
using System.Linq;
using AcystTechApi.MyServices;
using AcystTechApi.Extensions;

namespace AcystTechApi.Pages.Account
{
    [AllowAnonymous]
    public class ConfirmEmailModel : PageModel
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly AcystDbContext _context;
        private readonly IEmailSender _emailSender;

        public ConfirmEmailModel(UserManager<ApplicationUser> userManager, AcystDbContext context, IEmailSender emailSender)
        {
            _userManager = userManager;
            _context = context;
            _emailSender = emailSender;
        }

        public string Message { get; set; }

        public async Task<IActionResult> OnGet(string userId, string code)
        {
            if (userId == null || code == null)
            {
                Message = "User Id and code not supplied";
            }

            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                Message = ($"Unable to load user with ID '{userId}'.");
            }
            else
            {
                var result = await _userManager.ConfirmEmailAsync(user, code);
                if (!result.Succeeded)
                {
                    Message = ($"Error confirming email for user with ID '{userId}':");
                }
                else
                {
                    Message = $"User with email : {user?.UserName} confirmed successfully";
                    var use = _context.Users.Where(u => u.Username == user.UserName).FirstOrDefault();
                    var company = _context.CompanyDetails.Where(u => u.CompanyId == use.CompanyId).FirstOrDefault();

                    await _emailSender.SendEmailConfirmAsync(company.Url, use.Email, use.FullName);
                    //var Url = company.Url;
                    Response.Redirect(location: company.Url);
                }
            }

            return null;
        }

    }
}