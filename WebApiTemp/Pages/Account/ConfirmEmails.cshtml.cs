﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Authorization;
using AcystTechApi.Models;
using System.Linq;
using AcystTechApi.MyServices;
using AcystTechApi.Extensions;
using AcystTechApi.Models.BirthdayApp;
using Microsoft.EntityFrameworkCore;

namespace AcystTechApi.Pages.Account
{
    [AllowAnonymous]
    public class ConfirmEmailsModel : PageModel
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly BdayDbContext _context;
        private readonly IEmailSender _emailSender;

        public ConfirmEmailsModel(UserManager<ApplicationUser> userManager, BdayDbContext context, IEmailSender emailSender)
        {
            _userManager = userManager;
            _context = context;
            _emailSender = emailSender;
        }

        public string Message { get; set; }

        public async Task<IActionResult> OnGet(string userId, string code)
        {
            if (userId == null || code == null)
            {
                Message = "User Id and code not supplied";
            }

            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                Message = ($"Unable to load user with ID '{userId}'.");
            }
            else
            {
                var result = await _userManager.ConfirmEmailAsync(user, code);
                if (!result.Succeeded)
                {
                    Message = ($"Error confirming email for user with ID '{userId}':");
                }
                else
                {
                    Message = $"User with email : {user?.UserName} confirmed successfully";
                    var use = _context.AppUsers.Where(u => u.Username == user.UserName).FirstOrDefault();
                    await _emailSender.SendEmailConfirmAsync("http://whitelist.acyst.tech", use.Email, use.Username);
                    Response.Redirect("http://whitelist.acyst.tech");
                }
            }

            return null;
        }

    }
}