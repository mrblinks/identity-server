﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Authorization;
using IdentityServer.Models;
using System.Linq;
using IdentityServer.MyServices;
using IdentityServer.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace IdentityServer.Views.Account
{
    [AllowAnonymous]
    public class ConfirmEmailModel : PageModel
    {
        private readonly UserManager<AppUsers> _userManager;
        private readonly AppDbContext _context;
        private readonly IEmailSender _emailSender;
        public IConfiguration Configuration { get; }

        public ConfirmEmailModel(UserManager<AppUsers> userManager, AppDbContext context, IEmailSender emailSender, IConfiguration configuration)
        {
            _userManager = userManager;
            _context = context;
            _emailSender = emailSender;
            Configuration = configuration;
        }

        public string Message { get; set; }
        public object AppSettings { get; private set; }

        public async Task<IActionResult> OnGet(string userId, string code)
        {
            if (userId == null || code == null)
            {
                Message = "User Id and code not supplied";
            }

            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                Message = ($"Unable to load user with ID '{userId}'.");
            }
            else
            {
                var result = await _userManager.ConfirmEmailAsync(user, code);
                if (!result.Succeeded)
                {
                    Message = ($"Error confirming email for user with ID '{userId}':");
                }
                else
                {
                    Message = $"User with email : {user?.UserName} confirmed successfully";
                    var use = _context.Users.Where(u => u.UserName == user.UserName).Include(c=>c.User).FirstOrDefault();
                    // var company = _context.CompanyDetails.Where(u => u.CompanyId == use.CompanyId).FirstOrDefault();
                    var Url = "";
                    await _emailSender.SendEmailConfirmAsync(Url, use.Email, use.User.FullName);
                    //var Url = company.Url;
                    Response.Redirect(location: Url);
                }
            }

            return null;
        }

    }
}