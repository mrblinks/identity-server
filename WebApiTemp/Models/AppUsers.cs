﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.Models
{
    public class AppUsers : IdentityUser
    {
        public bool isLoggedIn { get; set; }
        public DateTime JoinDate { get; set; }
        public DateTime LoginTime { get; set; }
        public DateTime LogOutTime { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
    }
}
