﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.Models
{
    public class Branch
    {
        [Key]
        public int BranchId { get; set; }
        [Required]
        public string BranchName { get; set; }
        public string BranchCode { get; set; }
        public string Postal { get; set; }
        [Required]
        public string Telephone { get; set; }
        [Required]
        public string Region { get; set; }
        public int LocationId { get; set; }
        public int CountryId { get; set; }
        [Required]
        public string Address { get; set; }
        public string Status { get; set; }
        public int CreatedUserId { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CompanyId { get; set; }

        public Country Country { get; set; }
        public Location Location { get; set; }
        public Company Company { get; set; }

        public virtual ICollection<User> User { get; set; }
    }
}
