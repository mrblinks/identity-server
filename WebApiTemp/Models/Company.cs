﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.Models
{
    public class Company
    {
        [Key]
        public int CompanyId { get; set; }
        public string CompanyNo { get; set; }
        [Required]
        public string CompanyName { get; set; }
        public string Postal { get; set; }
        [Required]
        public string Logo { get; set; }
        public string Status { get; set; }
        public int CountryId { get; set; }
        [Required]
        public DateTime ExpiryDate { get; set; }
        public int? CreatedUserId { get; set; }
        public DateTime CreatedDate { get; set; }
        
        public Country Country { get; set; }

        public virtual ICollection<Branch> Branch { get; set; }
        public virtual ICollection<User> User { get; set; }
    }
}
