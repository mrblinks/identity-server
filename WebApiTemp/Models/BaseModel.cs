﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.Models
{
    public class BaseModel
    {
        public int CreatedUserId { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? ModifiedUserId { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int BranchId { get; set; }
        public int CompanyId { get; set; }
        
        public Branch Branch { get; set; }
        public Company Company { get; set; }
    }
}
