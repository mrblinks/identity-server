﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.Models
{
    public class Location :BaseModel
    {
        [Key]
        public int LocationId { get; set; }
        public string Name { get; set; }
    }
}
