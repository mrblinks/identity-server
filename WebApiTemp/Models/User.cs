﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.Models
{
    public class User : BaseModel
    {
        [Key]
        public int UserId { get; set; }
        [Required]
        public string FullName { get; set; }
        [Required]
        public string Email { get; set; }
        public string Image { get; set; }
        public string UserType { get; set; }
        public string Status { get; set; }
        public bool BranchUser { get; set; }
        [Required]
        public string Username { get; set; }
        [NotMapped]
        public string Password { get; set; }
    }
}
