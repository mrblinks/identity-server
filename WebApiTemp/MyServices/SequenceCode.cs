﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdentityServer.Models;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;

namespace IdentityServer.MyServices
{
    public class SequenceCode
    {
        private readonly AppDbContext _context;

        public SequenceCode(AppDbContext context)
        {
            _context = context;
        }

        
        public async Task<string> GetCode(string type)
        {
            Sequence sequence = await _context.Sequence.FirstOrDefaultAsync(a => a.Name.ToLower() == type.ToLower());

            if (sequence != null)
            {
                //string code = sequence.Prefix + sequence.Counter + sequence.Suffix;
                //int codeint = Convert.ToInt32(sequence.Counter); codeint++;

                string transcodenum = (sequence.Counter).ToString();
                int length = sequence.Length;
                int padnum = length - transcodenum.Length;
                string number = transcodenum.PadLeft(padnum + 1, '0');
                string code = sequence.Prefix + number + sequence.Suffix;

                sequence.Counter += 1;
                _context.Entry(sequence).State = EntityState.Modified;
                await _context.SaveChangesAsync();

                return code;
            }
            return null;
        }
        
    }
}
